package test;

public class song {
 private  String songno;
 private  String songname;
 private String singer;
 public song(){}
 public song ( String songno,String songname,String singer){
	 this.songno=songno;
	 this.songname=songname;
	 this.singer=singer;
 }
public String getSongno() {
	return songno;
}
public void setSongno(String songno) {
	this.songno = songno;
}
public String getSongname() {
	return songname;
}
public void setSongname(String songname) {
	this.songname = songname;
}
public String getSinger() {
	return singer;
}
public void setSinger(String singer) {
	this.singer = singer;
}
 public String toString(){
	 return "编号："+songno+"\t\t"+"歌曲名:"+songname+"\t"+"演唱者:"+singer;
 }
 public boolean equals(Object obj){ 
     if(this == obj){     
             return true ;         
     } 
     if(obj==null){ 
             return false; 
   }
    if(!(obj instanceof song)){ 
           return false ; 
    } 
   song per =( song) obj ; //向下转型
   if(this.songno.equals(per.songno) && (this.songname==per.songname )){ 
             return true ; 
    } else {     
             return false ;   
     } 
} 

}
