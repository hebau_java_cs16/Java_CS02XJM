
import java.util.*;

public class Test {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Random rand = new Random();
        int max = 0, min = 0;
        int score[] = new int[10];
        double man[][] = new double[5][2];
        for (int i = 0; i < 5; i++) {
            
            for (int j = 0; j < score.length; j++) {     //对每一个人
                int in = rand.nextInt(10);
                score[j] = in;
            }
            max = max(score);
            // System.out.println("" + max);
            min = min(score);
            // System.out.println("" + min);
                System.out.print("第" + (i+1) + "人的评分是          ");
                for (int n = 0; n < score.length; n++) {

                    System.out.print("  " + score[n]);

                }
            System.out.println("");
            int flag = 0, flag1 = 0;
            for (int m = 0; m < score.length; m++) {
                if (max == score[m] && flag == 0) {
                    score[m] = 0;
                    flag = 1;
                }
                if (min == score[m] && flag1 == 0) {
                    score[m] = 0;
                    flag1 = 1;
                }
            }
            man[i][0] = avge(score);
            man[i][1] = i;
        }
        for (int j = 0; j < man.length - 1; j++)
            for (int i = 0; i < man.length - j - 1; i++) {
                if (man[i][0] > man[i + 1][0]) {
                    double m = man[i][0];
                    man[i][0] = man[i + 1][0];
                    man[i + 1][0] = m;
                    double n = man[i][1];
                    man[i][1] = man[i + 1][1];
                    man[i + 1][1] = n;
                }

            }
        for (int i = 0; i < man.length; i++) {
            System.out.printf("编号为%.0f 成绩为%.2f\n", man[i][1] + 1, man[i][0]);
        }
    }

    public static int max(int x[]) {
        int max1 = x[0];
        for (int i = 0; i < x.length; i++) {
            if (max1 <= x[i]) {
                max1 = x[i];
            }
        }
        return max1;
    }

    public static int min(int x[]) {
        int min1 = x[0];
        for (int i = 0; i < x.length; i++) {
            if (min1 >= x[i]) {
                min1 = x[i];
            }
        }
        return min1;
    }

    public static double avge(int x[]) {
        int sum = 0;
        for (int i = 0; i < x.length; i++) {
            sum += x[i];
        }
        double ave = (double) sum / 8.0;
        return ave;
    }

}