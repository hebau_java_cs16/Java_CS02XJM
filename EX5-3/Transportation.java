
abstract class Transportation {
	String num;
	String model;
	String name;
	Transportation(){
		
	}
	Transportation(String num,String model,String name){
		this.num=num;
		this.model=model;
		this.name=name;
	}
	public abstract void transport();
}
