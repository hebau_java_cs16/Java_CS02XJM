package test;
class Employee1 extends Employee{
	 private String department;
	 private double monthsalary;
	 public Employee1(){};
	 public Employee1(String name,int age,String sex,String department,double monthsalary) {
		 super(name,age,sex);
		 this.department=department;
		 this.monthsalary=monthsalary;
	 }
	 public void setDepartment(String department) {
		 this.department=department;
	 }
	 public void setMonthsalary(double monthsalary) {
		 this.monthsalary=monthsalary;
	 }
	 public String getDepartment() {
		 return department;
	 }
	 public double getMonthsalary() {
		 return monthsalary;
	 }
	 public String toString() {
		 return super.toString()+"�������ţ�"+department+"\t"+"��н��"+monthsalary+"\t";
	 }
}