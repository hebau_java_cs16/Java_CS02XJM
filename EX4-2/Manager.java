package test;

class Manager extends Employee{
	 private String duty;
	 private double salary;
	 public Manager(){};
	 public Manager (String name,int age,String sex,String duty,double salary) {
		super(name,age,sex);
		 this.duty=duty;
		 this.salary=salary;
	 }
	 public void setDuty(String duty) {
		 this.duty=duty;
	 }
	 
	 public void setSalary(double salary) {
		 this.salary=salary;
	 }
	 public String getDuty() {
		 return duty;
	 }
	 public double getSalary() {
		 return salary;
	 }
	 public String toString(){
		 return super.toString()+"ְλ��"+duty+"\t"+"��н��"+salary+"\t";
		 
	 }
}