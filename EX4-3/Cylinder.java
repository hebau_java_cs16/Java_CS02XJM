
 class Cylinder extends Liti {
    private int r;
    double pi=3.14;
   private  int height ;
   public Cylinder() {}
public Cylinder(int r,int height) {
	this.r=r;
	this.height=height;
}

	public void setR(int r){
	  this.r=r;
}
	public void setHeight(int height){
	  this.height=height;
}
	public int getR(){
		  return r;
	  }
	  public int getHeight(){
		  return height;
	  }
	  
	 public  double getSurfacearea() {
		 return (2*pi*r*height+pi*r*r*2);
	 }
	 public  double getVolume() {
		 return pi*r*r*height;
}
 }
