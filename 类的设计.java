class Date {
		private int year;
		private int month;
		private int day;
		public Date () {
		}
		public Date(int year,int month,int day) {
			this.year=year;
			this.month=month;
			this.day=day;
		}
		public void geteyear(int year) {
			this.year=year;
		}
		public int setyear() {
			return year;
		}
		public void getetmonth(int month) {
			this.month=month;
		}
		public int setmonth() {
			return month;
		}
		public void getday(int day) {
			this.day=day;
		}
		public int setday() {
			return day;
		}
		public String toString() {
			return year+"-"+month+"-"+day;
		}
}
class Department {
	private  String departmentnumber;
	private  String departmentname;
	private  String post;
	public Department() {
		
	}
	public Department(String departmentnumber) {
		this.departmentnumber=departmentnumber;
	}
	public void setdepartmentnumber(String departmentnumber) {//部门编号
		this.departmentnumber=departmentnumber;
	}
	public String getDepartmentnumber() {
		return departmentnumber;
	}
	public String getDepartmentname() {//部门名称
		return departmentname;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}
	public void setPost(String  post) {//经理
		this.post = post;	
	}
	public String getPost() {
		return post;
	}
	public String toString() {
		return "部门编号"+departmentnumber+"部门名称："+departmentname;
	}
	public String TOString() {
		return "部门编号:"+departmentnumber+"部门名称："+departmentname+"\n 经理"+post;
	}
}
import java.util.Scanner;
public class Test {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("请输入职工人数：");
		int n=input.nextInt();
		System.out.println("请输入第一个部门名称和编号：");
		String s1=input.next();
		String s2=input.next();
		System.out.println("请输入部门人数：");
		int N=input.nextInt();
		int M=0;
		Date []worker1=new Date [n];//生日
		Workers []worker2=new Workers [n];
		Department []worker3=new Department [n];
		Date []worker4=new Date [n];//参加工作日期
	 	System.out.println("设置经理:");
	 	String post=input.next();
		String d,s;
		int j=0;
 		for(int i=0;i<n;i++) {
 			if(M==N) {
 	 			System.out.println("请输入下一个部门名称和编号：");
 	 			 d=input.next();
 	 			 s=input.next();
 	 		 	System.out.println("设置经理:");
 	 		 	String Post=input.next();
 	 		 	j++;
 	 		 	post=Post;M++;
 			}
 			else {
 				 d=s1;
 				 s=s2;
 				 M++;
 			}
 			System.out.println("请输入第"+(i+1)+"个职工的职工号");
 			String a=input.next();
 			System.out.println("请输入第"+(i+1)+"个职工的姓名");
 			String b=input.next();
 			System.out.println("请输入第"+(i+1)+"个职工的性别");
 			String c=input.next();
 			System.out.println("请输入第"+(i+1)+"个职工的生日");
 			int x=input.nextInt();
 			int y=input.nextInt();
 			int z=input.nextInt();
 			worker1[i]=new Date(x,y,z);
 			worker2[i]=new Workers(a,b,c);
 			worker3[i]=new Department(s);
 			worker3[i].setDepartmentname(d);
 			System.out.println("请输入第"+(i+1)+"个职工的参加工作时间");
 			int e=input.nextInt();
 			int f=input.nextInt();
 			int g=input.nextInt();
 			worker4[i]=new Date(e,f,g);
 			worker2[i].setbirthday(worker1[i]);
 			worker2[i].setdepartmentname(worker3[i]);
 			worker2[i].setworkdate(worker4[i]);
 			worker3[j].setPost(post);
 			System.out.println(worker2[i].toString());
		}
 		System.out.println("公司员工：");
 		for(int i=0;i<n;i++) {
 			System.out.println(worker2[i].toString());
 		}
 		for(int i=0;i<2;i++) {
 			System.out.println(worker3[i].TOString());
 		}
	}
}

class Workers {

	private String number;
	private String name;
	private String sex;
	private Department departmentname;
	private Date birthday;
	private Date workdate;
	public Workers () {
	}
	public Workers(String number,String name,String sex) {
		this.number=number;
		this.name=name;
		this.sex=sex;
	}
	public void setnumber(String number) {//职工号
		this.number=number;
	}
	public String getnumber() {
		return number;
	}
	public void setname(String name) {//姓名
		this.name=name;
	}
	public String getname() {
		return name;
	}
	public void setsex(String sex) {//性别
		this.sex=sex;
	}
	public String getsex() {
		return sex;
	}
	public void setbirthday(Date birthday) {//生日
		this.birthday=birthday;
	}
	public Date getbirthday() {
		return birthday;
	}
	public void setdepartmentname(Department departmentname) {//工作部门
		this.departmentname=departmentname;
	}
	public Department getdepartment() {
		return departmentname;
	}
	public void  setworkdate(Date workdate) {//工作日期
		this.workdate=workdate;
	} 
	public Date getworkdate() {
		return workdate;
	}
	public String toString() {
		return "职工号："+number+"姓名:"+name+"性别:"+sex+"生日"+birthday+departmentname+"参加工作时间:"+workdate;
	}
}


