package test;

 class Feeder {
   private String name;
   public Feeder(String name ){
	   this.name=name;
   }
   public void setName(String name){
	   this.name=name;
   }
   public String getName(){
	   return name;
   }
 
   public void feedAnimal(Animal animal[]){
	   for(int i=0;i<animal.length;i++){
		   animal[i].eat();
	   }
   }
}
